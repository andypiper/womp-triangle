# Womp Triangle

This is a trivial and silly project / thing. Thing. It is underserving of being termed a project. It will run anywhere you can serve an HTML file to a modern browser. 

"Womp Triangle" was a name suggested by [Sean Tilley](https://social.wedistribute.org/@deadsuperhero) as an alternative to "Fediverse" during a side chat at Fediforum about whether or not folks understood what a "Fediverse" is or could be or should be (...or, whatever). Triangles, gingerbread people and florps on chain were requested as features. 

Other people were also present in the chat, and obviously, I was the person who latched on to it and bought a domain and stuff. Just to make sure the extent of this tomfoolery is shared.

So. That happened.


## I built this with Glitch!

[Glitch](https://glitch.com) is a friendly community where millions of people come together to build web apps and websites.

- Need more help? [Check out their Help Center](https://help.glitch.com/) for answers to any common questions.

## LICENSE

MIT

